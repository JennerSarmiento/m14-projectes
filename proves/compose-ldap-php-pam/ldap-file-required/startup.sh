#! /bin/bash
#Jenner Sarmiento Mendoza
#Anyo:2022-2023
#Curs:2hisx
#EdT Escola del Treball


if [ -f /var/lib/ldap/.file-required ]
then
    /usr/sbin/slapd -d0
else
	touch /var/lib/ldap/.file-required
	/usr/sbin/slapd -d0
	echo "Inicialització BD ldap edt.org"
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f slapd.conf -F /etc/ldap/slapd.d
	slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
	/usr/sbin/slapd -d0
	exit 0
	
fi
