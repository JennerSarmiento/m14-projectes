# CLIENT SERVIDOR LDAP .FILE-REQUIRED
## Alumne: Jenner Sarmiento
## Any: 2022-2023
## Curs: 2HISX

### Descripció :
Volem que a partir d'un ordinador remot (exemple g02) podem conectar-nos a un ordinador que tingui un compose que tingui els docker engegats:
 * jennersarmiento/ldap22:file-required
 * a190074lr/ldap22:phpldapadmin

# Modificacions en els fitxers
 * Creacio docker-compose.yml
 
```
#Use ldap services:
services:
  ldap:
    image: jennersarmiento/ldap22:file-required
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - "2hisx"
    volumes:
      -  "ldap-config:/etc/ldap/slapd.d"
      -  "ldap-data:/var/lib/ldap"
  phpldapadmin:
    image: a190074lr/ldap22:phpldapadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports: 
      - "80:80"
    networks:
      - "2hisx"
networks:
  2hisx:
volumes:
  ldap-config:
  ldap-data:

```


 * startup.sh (jennersarmiento/ldap22:file-required):

```
#! /bin/bash
#Jenner Sarmiento Mendoza
#Anyo:2022-2023
#Curs:2hisx
#EdT Escola del Treball


if [ -f /var/lib/ldap/.file-required ]
then
    /usr/sbin/slapd -d0
else
	touch /var/lib/ldap/.file-required
	/usr/sbin/slapd -d0
	echo "Inicialització BD ldap edt.org"
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f slapd.conf -F /etc/ldap/slapd.d
	slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
	/usr/sbin/slapd -d0
	exit 0
	
fi
```

 * /etc/hosts (Client):
 ```
 <IP> ldap.edt.org
 ```

### Client: 

 * Modificació del serveis nscd nslcd ldap.conf
 
 * Revisar el pam del common-session. 

 * Modificació del /etc/hosts per tenir 
connectivitat amb el ordinador amb ldap.edt.org
 
 * A la hora de llençar el compose del servidor hem de reiniciar els serveis nscd nslcd


### Servidor

 * Llençar un compose amb phpldapadmin i ldap.edt.org amb els ports exportats i els volums per a la persistencia de dades.

 * ldap.edt.org: modificacio del startup (.file-required)
 *  phpldapadmin.edt.org: phpldapadmin

```
docker compose up -d
```

 * Després de llençar el compose comprovar al volum que hi ha creat el document  ocult .file-required

 * Comprovem amb un getent els usuaris que tenim:
 
```
root@g06:~# getent passwd
...
unix01:x:1001:1001::/home/unix01:/bin/sh
unix02:x:1002:1002::/home/unix02:/bin/sh
unix03:x:1003:1003::/home/unix03:/bin/sh
unix04:x:1004:1004::/home/unix04:/bin/sh
user02:x:1006:1006::/home/user02:/bin/bash
user01:x:1007:1007::/home/user01:/bin/bash
pau:*:5000:601:Pau Pou:/tmp/home/pau:
pere:*:5001:601:Pere Pou:/tmp/home/pere:
anna:*:5002:600:Anna Pou:/tmp/home/anna:
marta:*:5003:600:Marta Mas:/tmp/home/marta:
jordi:*:5004:601:Jordi Mas:/tmp/home/jordi:
admin:*:10:10:Administrador Sistema:/tmp/home/admin:
user01:*:7001:610:user01:/tmp/home/1asix/user01:
user02:*:7002:610:user02:/tmp/home/1asix/user02:
user03:*:7003:610:user03:/tmp/home/1asix/user03:
user04:*:7004:610:user04:/tmp/home/1asix/user04:
user05:*:7005:610:user05:/tmp/home/1asix/user05:
user06:*:7006:611:user06:/tmp/home/2asix/user06:
user07:*:7007:611:user07:/tmp/home/2asix/user07:
user08:*:7008:611:user08:/tmp/home/2asix/user08:
user09:*:7009:611:user09:/tmp/home/2asix/user09:
user10:*:7010:611:user10:/tmp/home/2asix/user10:
...
```
  * Dintre de PHPLDAPADMIN eliminem una entrada (ej: marta) i tanquem els dockers del servidor:
```
docker compose down
```
  * Després de reiniciar els servidors no el torna a crear degut a la persistència de dades que tñe degut als volums que hi vam posar al docker-compose.yml

/var/lib/docker/volumes/compose-ldap-php-pam_ldap-data/_data --> info

```
root@g16:/var/lib/docker/volumes/compose-ldap-php-pam_ldap-data/_data# ls -la
total 116
drwxr-xr-x 2 systemd-resolve systemd-network   4096 Nov 29 12:39 .
drwx-----x 3 root            root              4096 Nov 29 11:30 ..
-rw------- 1 systemd-resolve systemd-network 106496 Nov 29 12:38 data.mdb
-rw-r--r-- 1 root            root                 0 Nov 29 12:21 .file-required
-rw------- 1 systemd-resolve systemd-network   8192 Nov 29 12:54 lock.mdb
```

```

```