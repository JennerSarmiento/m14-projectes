# Repte AWS 3 Servidor Web (apache)
## Alumne: Jenner Sarmiento Mendoza
## Any: 2022-2023
## Curs: 2ASIX

### Repte 3 Generar un servidor web amb Apache

 * Apartat 3.1 Crear un servidor web amb Apache
 * Apartat 3.2 Gestionar Security Groups
 * Apartat 3.3 Afegir Docker a la màquina virtual i desplegar un servei
 * Apartat 3.4 Repetir el procediment usant una AMI d’altres distribucions

#### Creació del servidor web apache:
Primer hem de crear una nova instancia en el AWS. Descripció

```
Name and Tags: Repte-3_Apache

Aplication and OS Images (AMI) : Debian
    Debian 11 (HVM) SSD Volume Type
    Architecture: 64-bit (x86)

Instance Type:
    t2.micro (free)

Key_pair (login): x

```


### Gestionar Security Groups
Ara hem de fer la gestió del security groups per a la instancia...:

```
Security group name     Security group ID       Type    Protocol    Port range  Source  Description
launch-wizard-1	        sg-0ed4f78641ab287cd	HTTP	tcp	            80	    0.0.0.0/0	servei web
launch-wizard-1	        sg-0ed4f78641ab287cd	ssh	    tcp	            22	    0.0.0.0/0	servei ssh
```

### Conectem desde el terminal
* Recorda que necessitem el la key pair per poder accedir sino no podrem entrar (si eliminem la key pair no podrem entrar MAI MÉS a la instància.)

SYNTAXIS:

```
ssh -i <key_pair> [ec2-user|admin|fedora]@IP
```
ex:
```
ssh -i debian_pass.pem admin@23.20.212.89
```

Al estar connectat per SSH, assegurem que tenen connectivitat l instància de AWS amb el nostre ordinador (localhost):

```
nmap 23.20.212.89

```
```
Starting Nmap 7.80 ( https://nmap.org ) at 2022-11-17 10:06 CET
Nmap scan report for ec2-23-20-212-89.compute-1.amazonaws.com (23.20.212.89)
Host is up (0.11s latency).
Not shown: 998 filtered ports
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 8.56 seconds
```

Una altra confirmació és obtenir la pàgina web que té la instància AWS.

```
wget 23.20.212.89
```
```md
--2022-11-17 10:08:02--  http://23.20.212.89/
Connecting to 23.20.212.89:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 10701 (10K) [text/html]
Saving to: ‘index.html’

index.html                100%[=====================================>]  10.45K  --.-KB/s    in 0s      

2022-11-17 10:08:02 (56.2 MB/s) - ‘index.html’ saved [10701/10701]

```

Entrem en el buscador la ip de la màquina amb el port 80 i sortirà la pagina web que tindrà la AWS


## 2 Part Docker

Instal·lem docker dintre del ordinador AWS i fem una prova de docker, per exemple amb PAM:

```
docker run  --name pam.edt.org -h pam.edt.org --network 2hisx -it jennersarmiento/pam22:base
```

```
unix01@ip-172-31-21-114:~$ docker ps -a
```

```
CONTAINER ID   IMAGE                        COMMAND                  CREATED         STATUS                     PORTS     NAMES
8699a7734230   jennersarmiento/pam22:base   "/bin/sh -c /opt/doc…"   2 minutes ago   Exited (0) 2 minutes ago             pam.edt.org
```
