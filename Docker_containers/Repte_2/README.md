#Repte2: Container usant Apache
### Alumne: Jenner Sarmiento Mendoza
### Curs: 2hisx (2022-2023)

Fitxers:

  * Dockerfile
  * index.html
  * README.md

#### Generar imatge:

docker build -t jennersarmiento/repte:2 .

#### Executar de manera interactiva

docker run --name repte2 -h repte2 -p 80:80 -it jennersarmiento/repte:2 /bin/bash

#### Executar en detach

docker run --name repte2 -h repte2 -p 80:80 -d jennersarmiento/repte:2
