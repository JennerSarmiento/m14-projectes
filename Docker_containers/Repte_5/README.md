# REPTE 6
# LDAP AMB ENTRYPOINT
Implementar una imatge de servidor ldap configurable segons el paràmetre rebut.

S’executa amb un entrypoint que és un script que actuarà segons li indiquil’argument rebut:

 * crear un servidor ldap buit, sense dades d’usuari.
 * crear un servidor ldap inicialitzat amb les dades de l’organització de l’escola
(dades inicials).
 * engegar el servei ldap utilitzant les dades que perdurin d’execucions anteriors

### Pasos A Fer:
#### Modificació del Dockerfile:

```
# ldapserver 2022

FROM debian:latest
LABEL author="@edt ASIX Curs 2022"
LABEL subject="ldapserver 2022"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y procps iproute2 iputils-ping nmap tree slapd ldap-utils
RUN mkdir /opt/docker/
WORKDIR /opt/docker/
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
EXPOSE 389
```

### STARTUP.SH
El startup esta modificat per a que rebi argument segons els alrguments que donis especificats:

 * initdb → ho inicialitza tot de nou i fa el populate de edt.org

 * slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

 * start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la 	configuració. És a dir, engega el servei usant les dades ja existents.

 * slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada .

### Inici del Docker

#### Inici del Docker com a argument "initdb"

```
docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -d jennersarmiento/repte:5 initdb
```

#### Inici del Docker com a argument "slapd"

```
docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -d jennersarmiento/repte:5 slapd

docker exec -it repte5 ldapsearch -x -LLL -b "dc=edt,dc=org"
No such object (32)

```

#### Inici del Docker com a argument "start|edtorg|res"

```
docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -d jennersarmiento/repte:5 start

docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -d jennersarmiento/repte:5 edtorg

docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -d jennersarmiento/repte:5 

```
#### Inici del Docker com a argument "slapcat n(1|2|_)"

```
docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -it jennersarmiento/repte:5 slapcat 1 

docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -it jennersarmiento/repte:5 slapcat 2

docker run --rm --name repte5 -h repte5 --network 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -it jennersarmiento/repte:5 slapcat

```


