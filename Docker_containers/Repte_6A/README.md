# Repte 6A: Networks Variables i Secrets
## jennersarmiento ASIX-M06 2022-2023
### Escola del Treball (EdT)
Implementar un servidor LDAP on el nom de l’administrador de la base de dades de
l’escola sigui captat a través de variables d’entorn.

## Modificacio del startup:

Modifiquem el document startup.sh per a que faci un canvi segons el entorn:

```
#! /bin/bash
#Jenner Sarmiento Mendoza
#Anyo:2022-2023
#Curs:2hisx
#EdT Escola del Treball

echo "Inicialitzacio de BD ldap edt-org"

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
sed -i "s/Manager/$ADMIN/" slapd.conf
sed -i "s/rootpw secret/rootpw $PASSWD/" slapd.conf
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
```
Ara engegem el container amb la variable d'entorn ADMIN:

```
docker run --rm --name repte6 -h repte6 --network 2hisx -p389:389 -e ADMIN="Jenner" -e PASSWD="jenner" -d jennersarmiento/repte:6
```

```
docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
aebd64e9a234   jennersarmiento/repte:6   "/bin/sh -c /opt/doc…"   31 seconds ago   Up 31 seconds   0.0.0.0:389->389/tcp, :::389->389/tcp   repte6
```

I comprovem que funciona amb l'odre whoami`

```
ldapwhoami -vx -D "cn=Jenner,dc=edt,dc=org" -w jenner
ldap_initialize( <DEFAULT> )
dn:cn=Jenner,dc=edt,dc=org
Result: Success (0)

```
