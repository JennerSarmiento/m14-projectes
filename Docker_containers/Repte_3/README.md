# REPTE 3

## Utilització de volums en un servidor postgres 

Usar postgres (la nostra versió) i una base de dades persistent. Usar la imatge
postgres original amb populate i persistència de dades. Exemples de SQL injectat
usant volumes.

### ARXIUS NECESSARIS

   * Dockerfile
   * BBDD training (carpeta) amb el populate.sh

Trobem la image del postgres oficial a:

/library/postgres

### ENGEGAR EL CONTAINER POSTGRES

```
docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v postgres-data:/var/lib/postgresql/data -d jennersarmiento/repte:3
```

 * El creem amb el volum postgres-data per a la persistencia de dades.

Despres de que funcioni el container entrar-hi per comprovar que funciona.

```
docker exec -it postgres22  psql -U postgres -d training
```

Al entrar al training comprovar que tenim les taules.

```
training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# SELECT * FROM clientes WHERE rep_clie >105 AND limite_credito >50000;
 num_clie |     empresa     | rep_clie | limite_credito 
----------+-----------------+----------+----------------
     2101 | Jones Mfg.      |      106 |       65000.00
     2108 | Holm & Landis   |      109 |       55000.00
     2118 | Midwest Systems |      108 |       60000.00
(3 rows)

```

Després de comprovar que funciona la BBDD aturar el container i tornar a engegar:

