# DOCKER SWARM 2022-2023

#### manager node(ordinador) --workers
#### root    ordinador
#### tot el conjunt --> swarm cluster(conjunt)

#### token --> pasar algo (testimoni) (ex: entrada d'un concert)


### docker swarm init --> iniciar el docker

docker swarm join --token SWMTKN-1-47q3809mzthykcs634k4nuzzv1212p9recp2w4mii0vn7k5zkx-cspk16p3ymro7gwrs6s9awpo2 10.200.245.216:2377

docker swarm join --token -token- -ip:port-manager-


```
docker node

Usage:  docker node COMMAND

Manage Swarm nodes

Commands:
  demote      Demote one or more nodes from manager in the swarm
  inspect     Display detailed information on one or more nodes
  ls          List nodes in the swarm
  promote     Promote one or more nodes to manager in the swarm
  ps          List tasks running on one or more nodes, defaults to current node
  rm          Remove one or more nodes from the swarm
  update      Update a node

Run 'docker node COMMAND --help' for more information on a command.
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ 
```

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
qzp7s0e3rmppj6ncjv4ti7a0o     g10        Ready     Active                          20.10.18
xfrtidwra32gw79kde0ui61lz *   g16        Ready     Active         Leader           20.10.18
```


### Fem usar init per poder desplegar aplicacions al swarm es l'ordre --> docker stack (x.yml)

* engega serveis (containers dels que podem tenir rèpliques)

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack deploy -c compose.yml myapp
Creating network myapp_webnet
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_web
```

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack ps myapp
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
gk16ooxtvz4r   myapp_redis.1        redis:latest                      g16       Running         Running 50 seconds ago             
zdj63jhqsbyz   myapp_visualizer.1   dockersamples/visualizer:stable   g16       Running         Running 48 seconds ago             
ku4eohmvatsk   myapp_web.1          edtasixm05/getstarted:comptador   g16       Running         Running 45 seconds ago             
zmhbdo657sir   myapp_web.2          edtasixm05/getstarted:comptador   g10       Running         Running 39 seconds ago 
```

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack ls
NAME      SERVICES   ORCHESTRATOR
myapp     3          Swarm
```

### Mostrar els serveis de un stack

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
re2u9jxfwk9c   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
i93bcbak3l6l   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
odajs6ik2s93   myapp_web          replicated   2/2        edtasixm05/getstarted:comptador   *:30000->80/tcp
```

### Per eliminar un stack:

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack rm myapp
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

### Per poder iniciar el stack ha d'estar en el swarm

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack 
deploy    ls        ps        rm        services  
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack  deploy -c compose.yml app
Creating network app_webnet
Creating service app_web
Creating service app_redis
Creating service app_visualizer
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack ls
NAME      SERVICES   ORCHESTRATOR
app       3          Swarm
```

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack services app 
ID             NAME             MODE         REPLICAS   IMAGE                             PORTS
x2nut81jergc   app_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
pxa04awdjk3g   app_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
ynqec3n5m0g9   app_web          replicated   2/2        edtasixm05/getstarted:comptador   *:30001->80/tcp

```

#### stack desitjat --> asegura que el stack sigui el que es trobi en el fitxer compose.yml

```
a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker stack deploy -c compose.yml app 
Updating service app_redis (id: x2nut81jergc6ko0uwlnvab8b)
Updating service app_visualizer (id: pxa04awdjk3g653jbxn9usij0)
Updating service app_web (id: ynqec3n5m0g9pq8t1ueev8ji7)
```

a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker service ls
ID             NAME             MODE         REPLICAS   IMAGE                             PORTS
xk68ondvxlip   app_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
mujcvzg15lvd   app_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
iefb4kkwt52y   app_web          replicated   3/3        edtasixm05/getstarted:comptador   *:80->80/tcp

a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker service ls
ID             NAME             MODE         REPLICAS   IMAGE                             PORTS
xk68ondvxlip   app_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
mujcvzg15lvd   app_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
iefb4kkwt52y   app_web          replicated   3/3        edtasixm05/getstarted:comptador   *:80->80/tcp

a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker service ps app_web
ID             NAME        IMAGE                             NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
y83m2x9z4lym   app_web.1   edtasixm05/getstarted:comptador   g16       Running         Running 3 minutes ago             
5x25il2rab2r   app_web.2   edtasixm05/getstarted:comptador   g10       Running         Running 3 minutes ago             
wzcte76c8gho   app_web.3   edtasixm05/getstarted:comptador   g16       Running         Running 3 minutes ago   


a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker service scale app_web=2
app_web scaled to 2
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 


a201560js@g16:~/Documents/m14-projectes/Docker_Swarm/swarm$ docker service ls
ID             NAME             MODE         REPLICAS   IMAGE                             PORTS
xk68ondvxlip   app_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
mujcvzg15lvd   app_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
iefb4kkwt52y   app_web          replicated   4/2        edtasixm05/getstarted:comptador   *:80->80/tcp


a201560js@g16:~/Documents/m14-projectes$ docker stack rm app
Removing service app_redis
Removing service app_visualizer
Removing service app_web
Removing network app_webnet


SUMMARY (...WORKING...):

docker swarm
  join
  leave
docker node
  ls
  update
  rm
docker stack
  deploy
  ps
  rm
  ls
docker service
  ls
  ps
  scale -stakc_service-=n

docker ps

docker node ls






EXEMPLE SWARM -- GETTING STARTED WITH SWARM

1)CREACIÓ DEL SWARM I LA UNIÓ DE NODES

MANAGER:

a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker swarm init 
Swarm initialized: current node (lgo9ywq39p0hbznsbxu2mvud7) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0cllxrhajbbtgpv5ozbs53mg8lxydhjrkalsg96gfsmospn9px-afrbtdokr8vygpumkp994la1g 10.200.245.216:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.





MACHINE1/MACHINE2:

a201560js@g06:~$ docker swarm join --token SWMTKN-1-0cllxrhajbbtgpv5ozbs53mg8lxydhjrkalsg96gfsmospn9px-afrbtdokr8vygpumkp994la1g 10.200.245.216:2377
This node joined a swarm as a worker.

------------------------------------------------------------------------------------------------------------------------------------------------------

a201560js@g05:~$ docker swarm join --token SWMTKN-1-0cllxrhajbbtgpv5ozbs53mg8lxydhjrkalsg96gfsmospn9px-afrbtdokr8vygpumkp994la1g 10.200.245.216:2377
This node joined a swarm as a worker.



ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
kg9f1zm3i846tozb53ylhdtzz     g05        Down      Active                          20.10.18
pjh60ink5ye71qc0fheffxtd3     g06        Down      Active                          20.10.18
lgo9ywq39p0hbznsbxu2mvud7 *   g16        Ready     Active         Leader           20.10.18


2) CREACIÓ D'UN SERVEI:

a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service create --replicas 1 --name helloworld alpine ping docker.com
vyd45o4suay1o2feawzajsyh2
overall progress: 1 out of 1 tasks 
1/1: running   
verify: Service converged 



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service inspect --pretty helloworld 

ID:		vyd45o4suay1o2feawzajsyh2
Name:		helloworld
Service Mode:	Replicated
 Replicas:	1
Placement:
UpdateConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		alpine:latest@sha256:bc41182d7ef5ffc53a40b044e725193bc10142a1243f395ee852a8d9730fc2ad
 Args:		ping docker.com 
 Init:		false
Resources:
Endpoint Mode:	vip



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service ls
ID             NAME         MODE         REPLICAS   IMAGE           PORTS
vyd45o4suay1   helloworld   replicated   1/1        alpine:latest  



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service inspect helloworld 
[
    {
        "ID": "vyd45o4suay1o2feawzajsyh2",
        "Version": {
            "Index": 21
        },
        "CreatedAt": "2022-11-08T11:28:12.106047868Z",
        "UpdatedAt": "2022-11-08T11:28:12.106047868Z",
        "Spec": {
            "Name": "helloworld",
            .
            .
            .



201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service ps helloworld 
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
zk4fmycgu5oa   helloworld.1   alpine:latest   g16       Running         Running 6 minutes ago             



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker ps
CONTAINER ID   IMAGE           COMMAND             CREATED         STATUS         PORTS     NAMES
8a5aa4f170fd   alpine:latest   "ping docker.com"   6 minutes ago   Up 6 minutes             helloworld.1.zk4fmycgu5oadkwye19bbdex8


3)ESCALA EL SERVEI EN SWARM


a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service scale helloworld=5
helloworld scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   
2/5: running   
3/5: running   
4/5: running   
5/5: running   
verify: Service converged 


a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$  docker service ps helloworld 
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
zk4fmycgu5oa   helloworld.1   alpine:latest   g16       Running         Running 7 minutes ago              
yeogvhggf5fq   helloworld.2   alpine:latest   g16       Running         Running 19 seconds ago             
jemfoqmqpjpp   helloworld.3   alpine:latest   g16       Running         Running 19 seconds ago             
vucdvwxnockx   helloworld.4   alpine:latest   g16       Running         Running 18 seconds ago             
s7c1zinlh2qo   helloworld.5   alpine:latest   g16       Running         Running 19 seconds ago  



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker ps
CONTAINER ID   IMAGE           COMMAND             CREATED              STATUS              PORTS     NAMES
35254f5684f6   alpine:latest   "ping docker.com"   About a minute ago   Up About a minute             helloworld.5.s7c1zinlh2qoafze1nswymkc4
fb831f963c68   alpine:latest   "ping docker.com"   About a minute ago   Up About a minute             helloworld.2.yeogvhggf5fqr8frhweivypna
a65e72a5cba8   alpine:latest   "ping docker.com"   About a minute ago   Up About a minute             helloworld.4.vucdvwxnockx7y54jgle509w0
6f5f338345df   alpine:latest   "ping docker.com"   About a minute ago   Up About a minute             helloworld.3.jemfoqmqpjppwoih1om71o07q
8a5aa4f170fd   alpine:latest   "ping docker.com"   8 minutes ago        Up 8 minutes                  helloworld.1.zk4fmycgu5oadkwye19bbdex8



4) ELIMINAR EL SERVEI

a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service rm helloworld 
helloworld



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service inspect helloworld
[]
Status: Error: no such service: helloworld, Code: 1



5) APLICAR ACTUALITZACIONS CONTINUES EN UN SERVEI 



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service create   --replicas 3   --name redis   --update-delay 10s   redis:3.0.6
konczp3zmqmd9tkje6toeybnr
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$  docker service inspect --pretty redis

ID:		konczp3zmqmd9tkje6toeybnr
Name:		redis
Service Mode:	Replicated
 Replicas:	3
Placement:
UpdateConfig:
 Parallelism:	1
 Delay:		10s
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		redis:3.0.6@sha256:6a692a76c2081888b589e26e6ec835743119fe453d67ecf03df7de5b73d69842
 Init:		false
Resources:
Endpoint Mode:	vip



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$  docker service update --image redis:3.0.7 redis 
redis
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service inspect --pretty redis

ID:		konczp3zmqmd9tkje6toeybnr
Name:		redis
Service Mode:	Replicated
 Replicas:	3
UpdateStatus:
 State:		completed
 Started:	3 minutes ago
 Completed:	2 minutes ago
 Message:	update completed
Placement:
UpdateConfig:
 Parallelism:	1
 Delay:		10s
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		redis:3.0.7@sha256:730b765df9fe96af414da64a2b67f3a5f70b8fd13a31e5096fee4807ed802e20
 Init:		false
Resources:
Endpoint Mode:	vip



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker service ps redis
ID             NAME          IMAGE         NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
19adcmcg67n1   redis.1       redis:3.0.7   g16       Running         Running 3 minutes ago              
1z9w3qy2dzbk    \_ redis.1   redis:3.0.6   g16       Shutdown        Shutdown 3 minutes ago             
cg0q8wufeglq   redis.2       redis:3.0.7   g16       Running         Running 3 minutes ago              
sk67zaegx64r    \_ redis.2   redis:3.0.6   g16       Shutdown        Shutdown 3 minutes ago             
4efke8imd2qc   redis.3       redis:3.0.7   g16       Running         Running 3 minutes ago              
9dhk5d9ww8kl    \_ redis.3   redis:3.0.6   g16       Shutdown        Shutdown 3 minutes ago  


6) DRAIN UN NODE EN EL SWARM



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$  docker node update --availability drain g06
g06



a201560js@g16:~/Documents/m14-projectes/Docker_Swarm$ docker node inspect --pretty g06
ID:			pjh60ink5ye71qc0fheffxtd3
Hostname:              	g06
Joined at:             	2022-11-08 11:27:00.790538851 +0000 utc
Status:
 State:			Down
 Message:              	Heartbeat failure
 Availability:         	Drain
 Address:		10.200.245.206
Platform:
 Operating System:	linux
 Architecture:		x86_64
Resources:
 CPUs:			4
 Memory:		7.686GiB
Plugins:
 Log:		awslogs, fluentd, gcplogs, gelf, journald, json-file, local, logentries, splunk, syslog
 Network:		bridge, host, ipvlan, macvlan, null, overlay
 Volume:		local
Engine Version:		20.10.18
TLS Info:
 TrustRoot:
-----BEGIN CERTIFICATE-----
MIIBajCCARCgAwIBAgIUGrY8IAwrUSBC0oaE2Vx+Bb4Oy4IwCgYIKoZIzj0EAwIw
EzERMA8GA1UEAxMIc3dhcm0tY2EwHhcNMjIxMTA4MTEyMjAwWhcNNDIxMTAzMTEy
MjAwWjATMREwDwYDVQQDEwhzd2FybS1jYTBZMBMGByqGSM49AgEGCCqGSM49AwEH
A0IABL/RlXbLvWHKOqsisrB3xl1GZQwwtgcSwAwjmHDAak7kspNsU7qkGe3hDskX
jRQvJ0C8okmQl0FGptAQsI0X5MejQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMB
Af8EBTADAQH/MB0GA1UdDgQWBBRWvH1qMmqLYTtKL2gsY6sHchGdVTAKBggqhkjO
PQQDAgNIADBFAiBeJrRhwFk5x4rCso5KFF6mzBxchzYPi8CBkxrU5IAPxAIhANzl
Ac3N31cZMmdDTJGMwaEGyaqjr8OW3CcKWpdEiNUo
-----END CERTIFICATE-----

 Issuer Subject:	MBMxETAPBgNVBAMTCHN3YXJtLWNh
 Issuer Public Key:	MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEv9GVdsu9Yco6qyKysHfGXUZlDDC2BxLADCOYcMBqTuSyk2xTuqQZ7eEOyReNFC8nQLyiSZCXQUam0BCwjRfkxw==



When you set the node back to Active availability, it can receive new tasks:

during a service update to scale up
during a rolling update
when you set another node to Drain availability
when a task fails on another active node


QUE ÉS EL ROUTING MESH --> DOCKER 
El routuing mesh fa que quan publiquem un port d'un servei, aquest port es troba publicat per a tots els nodes del swarm.