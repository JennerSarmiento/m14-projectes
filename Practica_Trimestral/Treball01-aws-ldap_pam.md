# Configuració LDAP SERVER EN AWS 


Aquesta fase consisteix en implementar en els hosts locals de l’aula (mini debian)
l’autenticació LDAP. 

Els serveis LDAP es despleguen al cloud.

#### Especificacions:

Client Real:

- 10 Gb d'espai
- No Desktop Environment
- No Gnome
- Instal·lació en el sda2 (/dev/sda2)
- Modificar el arxiu de /etc/hosts per a que pugui tenir connectivitat amb el servidor AWS (!!!!! Teniu en compte que cada vegada que tornem a engegar la instancia de AWS, es canvia la IP):

```
127.0.0.1	localhost
127.0.1.1	g06.informatica.escoladeltreball.org	g06
50.17.151.10    ldap.edt.org

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

- Instal·lació dels paquets [slapd | ldap-utils].
```
sudo apt-get -y install slapd ldap-utils
```
- A la hora de loguejar i tenir directoris dintre del client (common-session)

```
session required	pam_mkhomedir.so
session	required	pam_mount.so 
```

- Instal·lació i configuració del ldap client (fent cambis en el arxius: | nsswitch.conf | ldap.conf | nslcd.conf)
```
Fer el canvis adient segons la base i el URI de la BBDD
```
- Iniciar els serveis nscd nslcd

```
systemctl start nscd
systenctl start nslcd
```

- Comprovació de conectivitat entre el ldap i el ordinador client real (getent passwd)

```
getent passwd
```

AWS (Amazon Web Services):

- Instancia en Debian11 amb la modificació del security groups permetent la sortida del port 22 (SSH) i el port 389(ldap).
- Dintre de la instancia; instal·lació del Docker i modificació dels usuaris que tindràn permisos (admin, guest, root)
- Fer correr un container amb la imatge LDAP ja instal·lada. (ex: jennersarmiento/ldap22:latest)

```
 docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -p389:389 -d jennersarmiento/ldap22:latest 
```

