# LDAP SERVER 2022
## jennersarmiento ASIX-M06 2022-2023
### SERVIDOR LDAP (Debian 11)
Podeu trobar la imatge del Docker al Dockerhub de [jennersarmiento](https://hub.docker.com/u/jennersarmiento)

Nom de la imatge: jennersarmiento/ldap22:base

ASIX M06-ASIX EdT (Escola del Treball Barcelona)
-----------------------------------------------------------------------------------------------------------------
#### Treure la imatge

```
docker pull jennersarmiento/repte:4
```

#### Iniciar la maquina virtual amb els volums:

```
docker run --name repte4 -h repte4 -v ldap-config:/etc/ldap/slapd.d -v ldap-info:/var/lib/ldap --network 2hisx -p 389:389 -d jennersarmiento/repte:4
```

#### Comprovació de que estigui encès el docker

```
docker ps

nmap 172.18.0.2
```

#### Exemple de comprovació (desde el ordinador host al docker):

```
ldapsearch -x -LLL -b 'dc=edt,dc=org'
```

#### Eliminem Anna Pou com a exemple per veure que hi ha consistencia de dades:

```
ldapdelete -vx -D 'cn=Manager,dc=edt,dc=org' -W 'cn=Anna Pou,ou=usuaris,dc=edt,dc=org'
ldap_initialize( <DEFAULT> )
Enter LDAP Password: 
deleting entry "cn=Anna Pou,ou=usuaris,dc=edt,dc=org"

ldapsearch -x -LLL 'dc=edt,dc=org' 'cn=Anna Pou'
No such object (32)

```
