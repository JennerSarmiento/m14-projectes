#! /bin/bash
#Jenner Sarmiento Mendoza
#Anyo:2022-2023
#Curs:2hisx
#EdT Escola del Treball
arg=$1
if [ $# -lt 1 ];
then
	arg="null"
fi

case $arg in
  "initdb")
    # inserta tota la base de dades sencera
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f slapd.conf -F /etc/ldap/slapd.d
    slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
    chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "slapd")
    #Només engega el servidor
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f slapd.conf -F /etc/ldap/slapd.d
    chown -R openldap.openldap /etc/ldap/slapd.d
    /usr/sbin/slapd -d0
    ;;
  "start"|"edtorg"|"null")
    #Engega utilitzant els volums.
    /usr/sbin/slapd -d0
    ;;	
  "slapcat")
    
    echo "SLAPCAT: $2"
    if [ $# -lt 2 ];then
      slapcat	
    else
      slapcat -n$2
    fi
    ;;
esac
exit 0

